package com.agiletestingalliance;


import org.junit.Test;
import static org.junit.Assert.*;

public class  MinMaxTest {
    @Test
    public void testFtm() throws Exception {
        int k = new MinMax().ftm(10,5);
        assertEquals("MaxValue", 10, k);
    }
    @Test
    public void testbar()  throws Exception {
        String res = new MinMax().bar("abc");
		assertEquals("label","abc",res);
    }
}