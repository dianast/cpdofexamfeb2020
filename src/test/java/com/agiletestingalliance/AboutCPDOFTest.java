package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutCPDOFTest{
    @Test
    public void testDesc()  throws Exception {
        String res = new AboutCPDOF().desc();
        assertTrue(res.contains("CP-DOF certification program covers end to end DevOps Life Cycle practically."));
    }
}