package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class DurationTest{
    @Test
    public void testDesc()  throws Exception {
        String res = new Duration().dur();
        assertTrue(res.contains("CP-DOF is designed specifically for corporates and working professionals alike."));
    }

    @Test
    public void testCalc()  throws Exception {
        new Duration().calculateIntValue();
        assertEquals("Sub", 5, 5);
    }
}