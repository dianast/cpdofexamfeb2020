package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class UsefulnessTest{
    @Test
    public void testDesc()  throws Exception {
        String res = new Usefulness().desc();
        assertTrue(res.contains("DevOps is about transformation, about building quality in"));
    }

    @Test
    public void testfunctionWhile() throws Exception {
        new Usefulness().functionWhile();
        assertEquals("funcWhile", 4, 4);
    }
}